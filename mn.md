# **Event handling in JavaScript**
```javascripts
function(){
    console.log("Hello~");
    }
```
## **Event handlers**

  - Browsers do this by allowing us to register functions
    as handlers for specific events.

  - The **window/document** binding refers to a built-in object provided
    by the browser. It represents the browser window that contains the
    document. Calling its **addEventListener** method registers the
    second argument to be called whenever the event described by its
    first argument occurs.

Example:

![](Image/media/image1.png)

## **Event Object**

  - All event objects in the DOM are based on the Event Object.

  - Therefore, all other event objects (like **MouseEvent** and
    **KeyboardEvent**) has access to the Event Object's properties and
    methods. Event handler functions are passed an argument: the event
    object.

Example:

![](Image/media/image2.png)

Output:

![](Image/media/image3.png)

## **Events List**

### **Keyboard Events** 

| Event Name  | Description                    |
| ----------  | ------------------------------ |
| keydown     | Event fire when pressing a key |
| keypress    | Event fire when press a key    |
| keyup       | Event fire when releases a key |

### **Mouse Events** 

| Event Name | Description                                              |
| ---------- | -------------------------------------------------------- |
| click      | Event fire when mouse click on element.                  |
| dblclick   | Event fire when mouse double click on element            |
| mousedown  | Event fire when mouse button is pressed down on element  |
| mouseup    | Event fire when mouse button is released over an element |
| mousemove  | Event fire when mouse pointer moves over an element      |
| mouseover  | Event fire when mouse pointer moves over on element.     |
| mousewheel | Event fire when mouse wheel being rotated.               |
| mouseout   | Event fire when mouse pointer moves out an element.      |

### **Window Events** 

| Event Name | Description                                        |
| ---------- | -------------------------------------------------- |
| load       | Event fires after the page loading finished        |
| unload     | Event fires when browser window has been closed    |
| scroll     | The document view or an element has been scrolled. |
| resize     | Event fires when browser window is resized         |
| error      | Script is run when any error occur                 |

### **Default actions**

  - JavaScript event handlers are called before the default behavior
    takes place. If the handler doesn’t want this normal behavior to
    happen, typically because it has already taken care of handling the
    event, it can call the **preventDefault** method on the event
    object.

**Example:**

![](Image/media/image4.png)

**Output:**

![](Image/media/image5.png)

### **Custom Event**

Example:

![](Image/media/image6.png)

Output:

![](Image/media/image7.png)
